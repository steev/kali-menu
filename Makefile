SIZES = 16 22 24 32 48 256
NAMES = $(basename $(notdir $(wildcard menu-icons/scalable/apps/*.svg)))

all:
	@$(MAKE) $(TARGETS)

install:

clean:

# Add dynamic rules to convert all SVG files into various PNG
define svg_to_png
menu-icons/$(1)x$(1)/apps/$(2).png: menu-icons/scalable/apps/$(2).svg
	rsvg-convert -o $$@ -w $(1) -h $(1) $$<

TARGETS += menu-icons/$(1)x$(1)/apps/$(2).png

endef

$(foreach SIZE,$(SIZES),$(foreach NAME,$(NAMES),$(eval $(call svg_to_png,$(SIZE),$(NAME))))) 
